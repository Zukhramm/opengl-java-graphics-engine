package com.thecrouchmode.vector;

import java.nio.FloatBuffer;
import java.util.Arrays;

import com.thecrouchmode.LWJGL.util.Buffers;

public final class Matrix4f{

	private final float[] values = new float[16];
	
	public static final Matrix4f identity = new Matrix4f(
			1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			0,0,0,1);
	
	public static final Matrix4f zero = new Matrix4f(
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,0);
	
	public static Matrix4f perspective(float vfov, float aspect, float zNear, float zFar){
		float frustl = zFar -zNear;
		float scale = 1f / (float) Math.tan(vfov / 2f);
		return new Matrix4f(
				scale/aspect, 0, 0, 0,
				0, scale, 0, 0,
				0, 0, -(zFar+zNear)/frustl, -2*zFar*zNear/frustl,
				0, 0, -1, 0);
	}
	
	public static Matrix4f orthographic(float right, float left, float top, float bottom, float near, float far){
			return new Matrix4f(
				2/(right-left), 0, 0, -(right+left)/(right-left),
				0, 2/(top-bottom), 0, -(top+bottom)/(top-bottom),
				0, 0, -2/(far-near), -(near+far)/(far-near),
				0, 0, 0, 1);
	}
	
	public static Matrix4f oblique(float theta,float phi){
		return new Matrix4f(
				1,0,(float)Math.tan(Math.PI-theta),0,
				0,1,(float)Math.tan(Math.PI-phi),0,
				0,0,1,0,
				0,0,0,1);
}
	
	public static Matrix4f translation(Vector3f translation){
		return translation(translation.x, translation.y, translation.z);
	}
	
	public static Matrix4f scale(float scale){
		return new Matrix4f(
				scale, 0, 0, 0,
				0, scale, 0, 0,
				0, 0, scale, 0,
				0, 0, 0, 1);
	}
	
	public static Matrix4f scale(double scale){
		return scale((float)scale);
	}
	
	public static Matrix4f translation(float x, float y, float z){
		return new Matrix4f(
				1,0,0,x,
				0,1,0,y,
				0,0,1,z,
				0,0,0,1);
	}
	
	public static Matrix4f lookAt(Vector3f pos, Vector3f target, Vector3f up){
		up = up.unit();
		Vector3f f = target.subtract(pos).unit();
		Vector3f s = f.cross(up).unit();
		Vector3f u = s.cross(f).unit();
		
//      return new Matrix4f(
//      		s.x, s.y, s.z, -s.dot(pos),
//      		u.x, u.y, u.z, -u.dot(pos),
//      		-f.x, -f.y, -f.z, f.dot(pos),
//      		0, 0, 0, 1f);
      
      return view(pos, target.subtract(pos), up);
      
	}
	
	public static Matrix4f view(Vector3f pos, Vector3f dir, Vector3f up){
		up = up.unit();
		Vector3f f = dir.unit();
		Vector3f s = f.cross(up).unit();
		Vector3f u = s.cross(f).unit();
		
      return new Matrix4f(
      		s.x, s.y, s.z, -s.dot(pos),
      		u.x, u.y, u.z, -u.dot(pos),
      		-f.x, -f.y, -f.z, f.dot(pos),
      		0, 0, 0, 1f);
      
	}
	
	public static Matrix4f rotation(float angle, Vector3f axis){
      return new Quaternion(angle, axis).rotationMatrix();
	}

	public static Matrix4f rotation(double angle, Vector3f axis){
      return new Quaternion(angle, axis).rotationMatrix();
	}
	
	public Matrix4f(float ... values){
		if(values.length != 16){
			throw new IllegalArgumentException("Constructor values do not match matrix size");
		}
		
		for(int i = 0; i < 4; i++){
			this.values[i] = values[i*4];
			this.values[i+4] = values[i*4+1];
			this.values[i+8] = values[i*4+2];
			this.values[i+12] = values[i*4+3];
		}
	}
	
	public Matrix4f transpose(){
		return new Matrix4f(values);
	}
	
	public Matrix4f multiply(Matrix4f other){
		float[] product = new float[16];
		
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				product[i*4+j] =
						get(i,0)*other.get(0, j) +
						get(i,1)*other.get(1, j) +
						get(i,2)*other.get(2, j) +
						get(i,3)*other.get(3, j);
			}
		}
		return new Matrix4f(product);
	}
	
	private Matrix4f add(Matrix4f m){
		float[] result = new float[16];
		for(int i = 0; i < 16; i++){
			result[i] = values[i]+m.index(i);
		}
		return new Matrix4f(result).transpose();
	}
	
	public Vector4f multiply(Vector4f vector){
		return new Vector4f(
				vector.dot(row(0)),
				vector.dot(row(1)),
				vector.dot(row(2)),
				vector.dot(row(3)));
	}
	
	public Vector4f multiply(Vector3f vector, float w){
		return multiply(new Vector4f(vector, w)).normalize();
	}
	
	public float get(int i, int j){
		return values[i+4*j];
	}
	
	public Vector4f row(int i){
		return new Vector4f(
				get(i, 0),
				get(i, 1),
				get(i, 2),
				get(i, 3));
	}
	
	public Vector4f column(int j){
		return new Vector4f(
				get(0, j),
				get(1, j),
				get(2, j),
				get(3, j));
	}
	
	public FloatBuffer buffer(){
		return Buffers.createBuffer(values);
	}
	
	private float index(int i){
		return values[i];
	}
	
	public String toString(){
		StringBuilder asString = new StringBuilder();
		for(int i = 0; i < 4; i++){
			asString.append("["+
					get(i, 0)+","+
					get(i, 1)+","+
					get(i, 2)+","+
					get(i, 3)+"]\n");
		}
		return asString.toString();
	}
	
	public static Matrix4f product(Matrix4f ... matrices){
		return product(Arrays.asList(matrices));
	}
	
	public static Matrix4f product(Iterable<Matrix4f> matrices){
		Matrix4f result = identity;
		for(Matrix4f m : matrices){
			result = result.multiply(m);
		}
		return result;
	}

	public static Matrix4f sum(Matrix4f ... matrices){
		return sum(Arrays.asList(matrices));
	}
	
	public static Matrix4f sum(Iterable<Matrix4f> matrices){
		Matrix4f result = Matrix4f.zero;
		for(Matrix4f m : matrices){
			result = result.add(m);
		}
		return result;
	}

}
