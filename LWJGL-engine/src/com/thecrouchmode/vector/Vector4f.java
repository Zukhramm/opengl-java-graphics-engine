package com.thecrouchmode.vector;

public final class Vector4f{
	public final float x;
	public final float y;
	public final float z;
	public final float w;
	
	public Vector4f(float x, float y, float z, float w){
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w;
	}
	
	public Vector4f(Vector3f vector, float w){
		this.x = vector.x;
		this.y = vector.y;
		this.z = vector.z;
		this.w = w;
	}

	public Vector3f xyz(){
		return new Vector3f(x, y, z);
	}
	
	public Vector4f normalize(){
		if(w == 1) return this;
		else return scale(1/w);
	}
	
	public float dot(Vector4f other){
		return
				x*other.x +
				y*other.y +
				z*other.z +
				w*other.w;
	}
	
	public float norm(){
		return (float) Math.sqrt(x*x+y*y+z*z+w*w);
	}	
	
	public Vector4f unit(){
		if(x*x+y*y+z*z+w*w == 0) throw new IllegalArgumentException("Cannot normalize zero vector");
		return scale((float)(1/Math.sqrt(x*x+y*y+z*z+w*w)));
	}
	
	private Vector4f scale(float f){
		return new Vector4f(x*f, y*f, z*f, w*f);
	}

	public String toString(){
		return "["+x+","+y+","+z+","+w+"]";
	}
}
