package com.thecrouchmode.vector;

import java.io.File;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.thecrouchmode.LWJGL.util.Buffers;

public final class Vector3f{
	public final float x;
	public final float y;
	public final float z;
	
	public final static Vector3f zero = new Vector3f(0, 0, 0);

	public final static Vector3f xUnit = new Vector3f(1, 0, 0);
	public final static Vector3f yUnit = new Vector3f(0, 1, 0);
	public final static Vector3f zUnit = new Vector3f(0, 0, 1);
	
	public Vector3f(float x, float y, float z){
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Vector3f(double x, double y, double z){
		this((float)x, (float)y, (float)z);
	}

	public Vector3f scale(float f){
		return new Vector3f(x*f, y*f, z*f);
	}
	
	public Vector3f scale(double d){
		return scale((float) d);
	}
	
	public Vector3f cross(Vector3f v){
		return new Vector3f(
				y*v.z-z*v.y,
				z*v.x-x*v.z,
				x*v.y-y*v.x);	
	}
	
	public Vector3f add(Vector3f v){
		return new Vector3f(x+v.x, y+v.y, z+v.z);
	}
	
	public Vector3f subtract(Vector3f v){
		return new Vector3f(x-v.x, y-v.y, z-v.z);
	}
	
	public float dot(Vector3f v){
		return x*v.x+y*v.y+z*v.z;
	}
	
	public float norm(){
		return (float) Math.sqrt(x*x+y*y+z*z);
	}
	
	public Vector3f unit(){
		if(x*x+y*y+z*z == 0) throw new IllegalArgumentException("Cannot normalize zero vector");
		return scale((float)(1/Math.sqrt(x*x+y*y+z*z)));
	}
	
	public String toString(){
		return "["+x+","+y+","+z+"]";
	}
	
	public List<Float> list(){
		List<Float> l = new ArrayList<>();
		l.add(x);
		l.add(y);
		l.add(z);
		return l;
	}
	
	public static Vector3f sum(Vector3f ... vectors){
		return sum(Arrays.asList(vectors));
	}
	
	public static float dot(Vector3f u, Vector3f v){
		return u.dot(v);
	}
	
	public static Vector3f cross(Vector3f u, Vector3f v){
		return u.cross(v);
	}
	
	public static Vector3f sum(Iterable<Vector3f> vectors){
		Vector3f result = new Vector3f(0,0,0);
		for(Vector3f v : vectors){
			result = result.add(v);
		}
		return result;
	}

	public Vector3f negative(){
		return scale(-1);
	}
	
	public Vector3f xz(){
		return new Vector3f(x, 0, z);
	}
	
	public Vector3f xy(){
		return new Vector3f(x, y, 0);
	}
	
	public Vector3f yz(){
		return new Vector3f(0, y, z);
	}
}
