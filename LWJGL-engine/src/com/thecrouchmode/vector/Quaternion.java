package com.thecrouchmode.vector;

import java.util.Arrays;

public final class Quaternion{
	public final float r, i, j, k;
	
	/**
	 * 
	 * @param r the real value of the quaternion
	 * @param i the value of the first imaginary component
	 * @param j the value of the second imaginary component
	 * @param k the value of the third imaginary component
	 */
	public Quaternion(float r, float i, float j, float k){
		this.r = r;
		this.i = i;
		this.j = j;
		this.k = k;
	}
	
	/**
	 * Creates a quaternion to represent a rotation
	 * around an axis.
	 *  
	 * @param angle Rotation angle around the axis
	 * @param axis The axis to rotate around
	 */
	public Quaternion(float angle, Vector3f axis){
		double sina = Math.sin(angle);
		axis = axis.unit();
		this.r = (float) Math.cos(angle);
		this.i = (float) (axis.x*sina);
		this.j = (float) (axis.y*sina);
		this.k = (float) (axis.z*sina);
	}
	
	public Quaternion(double angle, Vector3f axis){
		this((float) angle, axis);
	}
	
	
	public float norm(){
		return (float) Math.sqrt(r*r+i*i+j*j+k*k);
	}
	
	public float normSquared(){
		return r*r+i*i+j*j+k*k;
	}
	
	public Quaternion normalize(){
		return scale((float)(1/Math.sqrt(r*r+i*i+j*j+k*k)));
	}
	
	public Quaternion scale(float f){
		return new Quaternion(r*f, i*f, j*f, k*f);
	}
	
	public Quaternion conjugate(){
		return new Quaternion(r, -i, -j, -k);
	}
	
	public Quaternion inverse(){
	   return conjugate().scale(norm()*norm());
	}
	
	public Quaternion multiply(Quaternion q){
		return new Quaternion(
				r*q.r - i*q.i - j*q.j - k*q.k,
				r*q.i + i*q.r + j*q.k - k*q.j,
				r*q.j - i*q.k + j*q.r + k*q.i,
				r*q.k + i*q.j - j*q.i + k*q.r);
	}
	
	public static Quaternion product(Quaternion ... quats){
		return product(Arrays.asList(quats));
	}
	
	public static Quaternion product(Iterable<Quaternion> quats){
		Quaternion result = new Quaternion(0, Vector3f.xUnit);
		for(Quaternion q : quats){
			result = result.multiply(q);
		}
		return result;
	}
	
//	public Matrix4f rotationMatrix(){
//		return new Matrix4f(
//				1-2*j*j-2*k*k, 2*i*j-2*k*r, 2*i*k+2*j*r, 0,
//				2*i*j+2*k*r, 1-2*i*i-2*k*k, 2*j*k-2*i*r, 0,
//				2*i*k-2*j*r, 2*j*k+2*i*r, 1-2*i*i-2*j*j, 0,
//				0, 0, 0, 1);
//	}
	
	public Matrix4f rotationMatrix(){
		return new Matrix4f(
				r*r+i*i-j*j-k*k, 2*i*j-2*k*r, 2*i*k+2*j*r, 0,
				2*i*j+2*k*r, r*r-i*i+j*j-k*k, 2*j*k-2*i*r, 0,
				2*i*k-2*j*r, 2*j*k+2*i*r, r*r-i*i-j*j+k*k, 0,
				0, 0, 0, 1);
	}
	
	public String toString(){
		return r+" + "+i+"i + "+j+"j + "+k+"k";
	}

}
