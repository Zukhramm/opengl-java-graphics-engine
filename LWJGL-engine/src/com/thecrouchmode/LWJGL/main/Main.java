package com.thecrouchmode.LWJGL.main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
//import org.lwjgl.opengl.GL11;
//import org.lwjgl.opengl.GL15;
//import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.OpenGLException;
//import org.lwjgl.opengl.GL30;
import org.lwjgl.util.glu.GLU;

import com.thecrouchmode.LWJGL.util.Buffers;
import com.thecrouchmode.graphics.Light;
import com.thecrouchmode.graphics.Model;
import com.thecrouchmode.graphics.Renderer;
import com.thecrouchmode.graphics.Shader;
import com.thecrouchmode.graphics.Texture;
import com.thecrouchmode.level.Level;
import com.thecrouchmode.vector.Matrix4f;
import com.thecrouchmode.vector.Quaternion;
import com.thecrouchmode.vector.Vector3f;
import com.thecrouchmode.vector.Vector4f;

import static org.lwjgl.opengl.GL11.*;
//import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
//import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
//import static org.lwjgl.opengl.GL21.*;
import static org.lwjgl.opengl.GL30.*;
//import static org.lwjgl.opengl.GL31.*;
//import static org.lwjgl.opengl.GL32.*;
//import static org.lwjgl.opengl.GL33.*;
//import static org.lwjgl.opengl.GL40.*;
//import static org.lwjgl.opengl.GL41.*;
//import static org.lwjgl.opengl.GL42.*;
//import static org.lwjgl.opengl.GL43.*;


public class Main implements Runnable{

	public static int width = 800;
	public static int height = 600;

	private long time;

	public static void main(final String[] args){
		final Thread thread = new Thread(new Main());
		thread.run();
	}

	public void run(){
		try{
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.create();
			System.out.println("GL_VERSION: "+glGetString(GL_VERSION));
			Display.setTitle("tutorial");
			Renderer.init();
			Keyboard.create();
			Mouse.create();
			Mouse.setGrabbed(true);
			Keyboard.enableRepeatEvents(false);
		}
		catch (final LWJGLException e){
			e.printStackTrace();
		}

				
		Shader shader = null;
		try{
			shader = new Shader("./shaders/fragphong_vertex", "./shaders/fragphong_fragment", null);
		}
		catch(IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//generate VAO because?
		
		int vertexArrayID = glGenVertexArrays();
		glBindVertexArray(vertexArrayID);
		
		Model dragon = null;
		Model cube = null;
		Level level = null;
		
		try{
			dragon = new Model("./models/head.off", true);
			cube = new Model("./models/cube.off", false);
			level = new Level("./levels/testlevel.png");
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
		Texture texture = null;
		Texture material = null;
		Texture normal = null;
		try{
			glActiveTexture(Texture.glTexture(0));
			texture = new Texture("./textures/texture.png", "png");
			glActiveTexture(Texture.glTexture(1));
			material = new Texture("./textures/material.png", "png");
			glActiveTexture(Texture.glTexture(2));
			normal = new Texture("./textures/shinymat.png", "png");
		}
		catch(IOException e){
			e.printStackTrace();
		}
		//Texture.unbind();
		
		Matrix4f projection = Matrix4f.perspective((float)Math.PI/8, 4f/3, 0.1f, 10000f);
		
		//projection = Matrix4f.orthographic(1.33f, -1.33f, 1, -1, 0.1f, 100f);
		//projection = projection.multiply(Matrix4f.oblique((float)Math.PI/4, (float)Math.PI/8));
		
		System.out.println(projection);
		
		Matrix4f scale = Matrix4f.scale(1);
		Matrix4f translation = Matrix4f.translation(0,0,0);
		Matrix4f rotation = Matrix4f.identity;
		//Matrix4f model = translation.multiply(scale);
		Quaternion zrotation = new Quaternion(-Math.PI/360, new Vector3f(0,1,2));
		Quaternion rotQ = zrotation;
		
		float ztrans = 0;
		float xtrans = 0;
		
		Vector3f camForw = Vector3f.zUnit;
		Vector3f camSide = Vector3f.xUnit.scale(-1);
		
		double value = 0;
		
		time = System.currentTimeMillis();
		int fpsMod = 0;
		
		Light light = Light.builder()
				.intensity(255/1, 255/2, 255/3)
				.pos(1, 1, 1).build();
		shader.setUniform("lightPos", light.pos.xyz());
		shader.setUniform("lightIntensity", light.intensity);
		
		Matrix4f view = Matrix4f.view(
				new Vector3f(0, 0, -5),
				camForw,
				Vector3f.cross(camSide, camForw));
		
		float camX = 0;
		float camZ = 0;
		
		Vector3f trans = new Vector3f(1, 0.5, -4);
		
		while(true){
			try{
				if(Display.isCloseRequested()){
					Display.destroy();
					Keyboard.destroy();
					//cleanup
					break;
				}
				else{
					
					Keyboard.poll();
					//Mouse.poll();
					//System.out.println("Mouse: " +Mouse.getDX());

					//view = Matrix4f.product(Matrix4f.rotation(Mouse.getDX()/(1000*Math.PI), Vector3f.yUnit), view);
					//view = Matrix4f.product(Matrix4f.rotation(-Mouse.getDY()/(1000*Math.PI), Vector3f.xUnit), view);
					
					

					
					while(Keyboard.next()){
						int keyNo = Keyboard.getEventKey();
						char character = Keyboard.getEventCharacter();
						boolean down = Keyboard.getEventKeyState();
						
						//System.out.print("Key " + keyNo + "(" + character +") ");
						
						float moveSpeed = 0.1f;
						if(keyNo == 17 && down){
							xtrans += moveSpeed;
						}
						if(keyNo == 30 && down){
							ztrans -= moveSpeed;
						}
						if(keyNo == 31 && down){
							xtrans -= moveSpeed;
						}
						if(keyNo == 32 && down){
							ztrans += moveSpeed;
						}
						
						if(keyNo == 17 && !down){
							xtrans -= moveSpeed;
						}
						if(keyNo == 30 && !down){
							ztrans += moveSpeed;
						}
						if(keyNo == 31 && !down){
							xtrans += moveSpeed;
						}
						if(keyNo == 32 && !down){
							ztrans -= moveSpeed;
						}
					}
					
					trans = Vector3f.sum(
							trans,
							camForw.xz().unit().scale(xtrans),
							camSide.xz().unit().scale(ztrans));
					
//					Matrix4f camRot = Quaternion.product(
//									new Quaternion(Mouse.getDY()/(600*Math.PI), camSide),
//									new Quaternion(-Mouse.getDX()/(600*Math.PI), Vector3f.yUnit)).normalize().rotationMatrix();
					
					Matrix4f camRot = Quaternion.product(
							new Quaternion(-Mouse.getDX()/(600*Math.PI), Vector3f.yUnit),
							new Quaternion(Mouse.getDY()/(600*Math.PI), camSide)).normalize().rotationMatrix();
					
					view = Matrix4f.product();
					camForw = camRot.multiply(camForw, 1).xyz();
					camSide = camRot.multiply(camSide, 1).xyz();
					
					view = Matrix4f.view(
							trans,
							camForw,
							Vector3f.cross(camSide, camForw));
					
					value = value+0.01;	
					
					Renderer.clear();


//					light.pos = new Vector4f(
//							1+3*(float)Math.sin(-2*value),
//							1+1*(float)Math.cos(-2*value),
//							1f,//,
//							1.0f);
					
					light.pos = new Vector4f(trans, 1.0f);

					
					shader.use();
					
					shader.setUniform("lightPos", light.pos.xyz());
					shader.setUniform("lightInt", light.intensity);
					shader.setUniform("flip", 0);
					
					//rotQ = rotQ.multiply(zrotation);//.normalize();
					rotation = rotQ.rotationMatrix();
					
					translation = Matrix4f.translation(0, 0, 0);
					Matrix4f m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					Matrix4f vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);	
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 2);
					
					texture.bind();
					
					dragon.draw();
					
					translation = Matrix4f.translation(2, 0, 0);
					m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("shiny", dragon.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
					
					dragon.draw();
					
					translation = Matrix4f.translation(0, 2, 0);
					m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("shiny", dragon.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
					
					dragon.draw();
					
					translation = Matrix4f.translation(0, 0, 2);
					m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("shiny", dragon.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
						
					dragon.draw();
					
					translation = Matrix4f.translation(0, 2, 2);
					m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("shiny", dragon.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
					
					dragon.draw();
					
					translation = Matrix4f.translation(2, 0, 2);
					m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("shiny", dragon.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
					
					dragon.draw();
					
					translation = Matrix4f.translation(2, 2, 0);
					m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("shiny", dragon.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
					
					dragon.draw();
					
					translation = Matrix4f.translation(2, 2, 2);
					m = Matrix4f.product(translation, rotation, scale, dragon.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", dragon.ambient);
					shader.setUniform("kDiffuse", dragon.diffuse);
					shader.setUniform("kSpecular", dragon.specular);
					shader.setUniform("shiny", dragon.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
						
					dragon.draw();
					
					translation = Matrix4f.translation(0, -0, 0);
					m = Matrix4f.product(translation, Matrix4f.scale(1), cube.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", cube.ambient);
					shader.setUniform("kDiffuse", cube.diffuse);
					shader.setUniform("kSpecular", cube.specular);
					shader.setUniform("shiny", cube.shinyness);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
						
					level.model.draw();
					
					translation = Matrix4f.translation(light.pos.xyz());
					m = Matrix4f.product(translation, Matrix4f.scale(0.1), cube.normalization);
					vp = Matrix4f.product(projection, view);
					shader.setUniform("M", m);
					shader.setUniform("VP", vp);
					shader.setUniform("kAmbient", cube.ambient);
					shader.setUniform("kDiffuse", cube.diffuse);
					shader.setUniform("kSpecular", cube.specular);
					shader.setUniform("shiny", cube.shinyness);
					shader.setUniform("flip", 1);
					shader.setUniform("texSampler", 0);
					shader.setUniform("matSampler", 1);
					
					cube.draw();
					
					
					
					long posttime = System.currentTimeMillis();
					long delta = posttime - time;
					time = posttime;

					//if(fpsMod%40 == 0) System.out.println(1000.0/delta);
					fpsMod++;
					Display.update();
					Thread.sleep(16);
				}

			}
			catch (final InterruptedException e){
				e.printStackTrace();
			}
		}
	}
}
