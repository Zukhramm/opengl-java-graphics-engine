package com.thecrouchmode.LWJGL.main;

import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glGetString;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import com.thecrouchmode.graphics.Light;
import com.thecrouchmode.graphics.Model;
import com.thecrouchmode.graphics.Renderer;
import com.thecrouchmode.level.Level;
import com.thecrouchmode.vector.Matrix4f;
import com.thecrouchmode.vector.Quaternion;
import com.thecrouchmode.vector.Vector3f;
import com.thecrouchmode.vector.Vector4f;

public class Sandbox{

	public static void main(String[] args){
		
		System.out.println();
		System.out.println(Vector3f.sum(new ArrayList<Vector3f>()));
		System.out.println();
		
		Vector3f pos = new Vector3f(4, 3, 3);
		
		Vector3f target = new Vector3f(0,0,0);
		
		Vector3f up = new Vector3f(0,1,0);

		Matrix4f view = Matrix4f.lookAt(pos, target, up);
		Matrix4f projection = Matrix4f.perspective((float)Math.PI/2, 1, 1, 100);
		Matrix4f model = Matrix4f.translation(42, 23, 0);
		Matrix4f model2 = Matrix4f.translation(42, 41, 314);
		
		
		Matrix4f MVP_manual = view.multiply(projection.multiply(model.multiply(model2)));
		Matrix4f MVP_loop = Matrix4f.product(view, projection, model, model2);
		
		System.out.println(MVP_manual);
		System.out.println(MVP_loop);
		
		System.out.println();
		
		System.out.println(Matrix4f.sum(Matrix4f.identity, new Matrix4f(
				4, 0, 0, 1,
				0, 0, 2, 0,
				0, 3, 0, 0,
				4, 0, 0, 1)));
		
		System.out.println();
		
		Quaternion p = new Quaternion(1, 2, 3, 4);
		Quaternion q = new Quaternion(3, 5, 6, 4);
		
		System.out.println(p.conjugate().conjugate());
		System.out.println(p.multiply(q));
		System.out.println(q.multiply(p));
		System.out.println(p.multiply(q).conjugate());
		System.out.println(q.conjugate().multiply(p.conjugate()));

		Quaternion r = p.normalize();
	
		System.out.println(1-2*r.j*r.j-2*r.k*r.k);
		System.out.println(r.r*r.r+r.i*r.i-r.j*r.j-r.k*r.k);
		
		System.out.println(1-2*r.i*r.i-2*r.k*r.k);
		System.out.println(r.r*r.r-r.i*r.i+r.j*r.j-r.k*r.k);
		
		System.out.println(1-2*r.i*r.i-2*r.j*r.j);
		System.out.println(r.r*r.r-r.i*r.i-r.j*r.j+r.k*r.k);
		
		System.out.println();
		Vector3f axis = new Vector3f(43, 4, 4);
		System.out.println(axis);
		Matrix4f rotation = new Quaternion(2, axis).rotationMatrix();
		//System.out.println(Matrix4f.identity.multiply(axis));
		
		System.out.println();
		
		System.out.println(new Quaternion(4, new Vector3f(0,1,3).unit()).rotationMatrix());
		System.out.println(new Quaternion(4, new Vector3f(0,1,3)).rotationMatrix());
		
		System.out.println();
		
		System.out.println(new Quaternion(4, new Vector3f(0,3,0)).rotationMatrix());
		System.out.println(new Quaternion(4, new Vector3f(0,3,0)).rotationMatrix());
	
	
		System.out.println();
		
		Vector4f a4 = new Vector4f(43, 1, 5, 6);

		Vector4f b4 = new Vector4f(4, 2, 67, 2);
		
		System.out.println(MVP_manual.multiply(a4).normalize());

		System.out.println(MVP_manual.multiply(a4.normalize()).normalize());
		
		Light lightsource = Light.builder()
				.pos(1, 3, 2)
				.intensity(1, 0, 0)
				.build();	
		
	}

}
