package com.thecrouchmode.LWJGL.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Config{

	private final Pattern sectionPattern = Pattern.compile("\\s*\\[([^]]*)\\]\\s*");
	private final Pattern keyValuePattern = Pattern.compile("\\s*([^=]*)=(.*)");
	private final Map<String, Map<String, String>> entries = new HashMap<>();
	private final String path;

	public Config(String path) throws IOException{
		load(path);
		this.path = path;
	}

	private void load(String path) throws IOException{
		try(BufferedReader br = new BufferedReader(new FileReader(path))){
			String line;
			String section = null;
			while((line = br.readLine()) != null){
				Matcher m = sectionPattern.matcher(line);
				if(m.matches()) section = m.group(1).trim();
				else if(section != null){
					m = keyValuePattern.matcher(line);
					if(m.matches()){
						final String key = m.group(1).trim();
						final String value = m.group(2).trim();
						Map<String, String> kv = entries.get(section);
						if(kv == null) entries.put(section, kv = new HashMap<>());
						kv.put(key, value);
					}
				}
			}
		}
	}
	
	/**
	 * does some thing cool huh?
	 * @return
	 */
	public Set<String> getSections(){
		return entries.keySet();
	}
	
	public Section getSection(String section){
		if(!entries.containsKey(section)){
			throw new IllegalArgumentException("No section " + section +  " in file " + path);
		}
		return new Section(section);
	}
	
	public Set<String> getEntries(String section){
		return entries.get(section).keySet();
	}
	
	public boolean hasEntry(String section, String key){
		return entries.get(section).containsKey(key);
	}

	public String getValue(String section, String key){
		final Map<String, String> kv = entries.get(section);
		if(kv == null){
			throw new IllegalArgumentException("No section " + section +  " in file " + path);
		}

		return kv.get(key);
	}

	public int asInt(String section, String key){
		return Integer.parseInt(getValue(section, key));
	}
	
	public double asDouble(String section, String key){
		return Double.parseDouble(getValue(section, key));
	}
	
	public boolean asBool(String section, String key){
		return Boolean.parseBoolean(getValue(section, key));
	}
	
	public List<String> asList(String section, String key){
		return Arrays.asList(getValue(section, key).split(", "));
	}
	
	public long[] asLongArray(String section, String key){
		String[] values = getValue(section, key).split(", ");
		long[] longs = new long[values.length];
		for(int i=0; i<values.length;i++){
			longs[i] = (long) Integer.parseInt(values[i]);
		}
		return longs;
	}
	
	public List<Integer> asIntList(String section, String key){
		String[] values = getValue(section, key).split(", ");
		Integer[] ints = new Integer[values.length];
		for(int i=0; i<values.length;i++){
			ints[i] = Integer.parseInt(values[i]);
		}
		return Arrays.asList(ints);
	}
	
	public List<Double> asDoubleList(String section, String key){
		String[] values = getValue(section, key).split(", ");
		Double[] dubs = new Double[values.length];
		for(int i=0; i<values.length;i++){
			dubs[i] = Double.parseDouble(values[i]);
		}
		return Arrays.asList(dubs);
	}
	
	public String getValue(String section, String key, String def){
		if(hasEntry(section, key)){
			return getValue(section, key);
		}
		else return def;
	}

	public int asInt(String section, String key, int def){
		if(hasEntry(section, key)){
			return asInt(section, key);
		}
		else return def;
	}
	
	public double asDouble(String section, String key, double def){
		if(hasEntry(section, key)){
			return asDouble(section, key);
		}
		else return def;
	}
	
	public boolean asBool(String section, String key, boolean def){
		if(hasEntry(section, key)){
			return asBool(section, key);
		}
		else return def;
	}
	
	public List<String> asList(String section, String key, List<String> def){
		if(hasEntry(section, key)){
			return asList(section, key);
		}
		else return def;
	}
	
	public long[] asLongArray(String section, String key, long[] def){
		if(hasEntry(section, key)){
			return asLongArray(section, key);
		}
		else return def;
	}
	
	public List<Integer> asIntList(String section, String key, List<Integer> def){
		if(hasEntry(section, key)){
			return asIntList(section, key);
		}
		else return def;
	}
	
	public List<Double> asDoubleList(String section, String key, List<Double> def){
		if(hasEntry(section, key)){
			return asDoubleList(section, key);
		}
		else return def;
	}
	
	public class Section{
		final private String section;
		private Section(String section){
			this.section = section;
		}
		
		public boolean hasEntry(String key){
			return Config.this.hasEntry(section, key);
		}

		public String getValue(String key){
			return Config.this.getValue(section, key);
		}

		public int asInt(String key){
			return Config.this.asInt(section, key);
		}
		
		public double asDouble(String key){
			return Config.this.asDouble(section, key);
		}
		
		public boolean asBool(String key){
			return Config.this.asBool(section, key);
		}
		
		public List<String> asList(String key){
			return Config.this.asList(section, key);
		}
		
		public long[] asLongArray(String key){
			return Config.this.asLongArray(section, key);
		}
		
		public List<Integer> asIntList(String key){
			return Config.this.asIntList(section, key);
		}
		
		public List<Double> asDoubleList(String key){
			return Config.this.asDoubleList(section, key);
		}
		
		public String getValue(String key, String def){
			return Config.this.getValue(section, key, def);
		}

		public int asInt(String key, int def){
			return Config.this.asInt(section, key, def);
		}
		
		public double asDouble(String key, double def){
			return Config.this.asDouble(section, key, def);
		}
		
		public boolean asBool(String key, boolean def){
			return Config.this.asBool(section, key, def);
		}
		
		public List<String> asList(String key, List<String> def){
			return Config.this.asList(section, key, def);
		}
		
		public long[] asLongArray(String key, long[] def){
			return Config.this.asLongArray(section, key, def);
		}
		
		public List<Integer> asIntList(String key, List<Integer> def){
			return Config.this.asIntList(section, key, def);
		}
		
		public List<Double> asDoubleList(String key, List<Double> def){
			return Config.this.asDoubleList(section, key, def);
		}
	}
}
