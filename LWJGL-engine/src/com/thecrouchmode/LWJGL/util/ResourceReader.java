package com.thecrouchmode.LWJGL.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ResourceReader{
	public static List<String> readLines(String path) throws IOException{
		try(BufferedReader reader = new BufferedReader(new FileReader(path))){
			List<String> lines = new ArrayList<>();
			String line;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}
			return lines;
			
		}
		catch(IOException e){
			throw new IOException("Unable to read lines from file "+path);
		}
	}
}