package com.thecrouchmode.LWJGL.util;

import java.util.Arrays;
import java.util.Iterator;

public class Bag<T> implements Iterable<T>{

	public Object[] data;
	private int size = 0;

	public Bag(){
		this(8);
	}

	public Bag(int capacity){
		data = new Object[capacity];
	}

	public int size(){
		return size;
	}

	@SuppressWarnings("unchecked")
	public T get(int index){
		if(index < 0 || index >= size){
			throw new IllegalArgumentException("illegal index " + index
					+ " Size: " + size);
		}
		return (T) data[index];
	}

	public void add(T entry){
		if(size == data.length){
			data = Arrays.copyOf(data, 2 * data.length);
		}
		data[size] = entry;
		size++;
	}

	public void remove(int index){
		if(index < 0 || index >= size){
			throw new IllegalArgumentException("illegal index " + index
					+ " Size: " + size);
		}

		if(index == size - 1){
			size--;
		}else{
			data[index] = data[size - 1];
			size--;
		}
	}

	public void remove(int start, int end){
		if(start < 0 || end >= size && start < end){
			throw new IllegalArgumentException("index out of bounds");
		}

		if(end == size - 1){
			size -= end - start + 1;
		}else{
			for(int i = 0; i <= end - start + 1; i++){
				data[start + i] = data[size - (end - start + 1) + i];

			}
			size -= end - start + 1;
		}
	}

	public Iterator<T> iterator(){
		return new Iterator<T>(){
			int index = 0;
			public boolean hasNext(){
				return index<size;
			}

			@SuppressWarnings("unchecked")
			public T next(){
				return (T) data[index++];
			}

			public void remove(){
				Bag.this.remove(index-1);
			}
		};
	}
}
