package com.thecrouchmode.LWJGL.util;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import org.lwjgl.BufferUtils;

public class Buffers{
	public static FloatBuffer createBuffer(double ... doubles){
		FloatBuffer buffer = BufferUtils.createFloatBuffer(doubles.length);
		for(double v : doubles){
			buffer.put((float) v);
		}
		buffer.flip();
		return buffer;
	};
	
	public static FloatBuffer createBuffer(float ... floats){
		FloatBuffer buffer = BufferUtils.createFloatBuffer(floats.length);
		for(float v : floats){
			buffer.put(v);
		}
		buffer.flip();
		return buffer;
	}

	public static IntBuffer createBuffer(int[] ints){
		IntBuffer buffer = BufferUtils.createIntBuffer(ints.length);
		for(int i : ints){
			buffer.put(i);
		}
		buffer.flip();
		return buffer;
	};
	
	public static float[] floatArrayFromList(List<Float> l){
		float[] array = new float[l.size()];
		for(int i = 0; i < array.length; i++){
			array[i] = l.get(i);
		}
		return array;
	}
	
	public static int[] intArrayFromList(List<Integer> l){
		int[] array = new int[l.size()];
		for(int i = 0; i < array.length; i++){
			array[i] = l.get(i);
		}
		return array;
	}
}
