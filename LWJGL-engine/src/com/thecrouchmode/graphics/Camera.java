package com.thecrouchmode.graphics;

import com.thecrouchmode.vector.Matrix4f;
import com.thecrouchmode.vector.Vector3f;

public class Camera{
	private Vector3f position;
	private Vector3f direction;
	private Vector3f up;
	
	public Camera(){
		position = new Vector3f(0,0,0);
		direction = new Vector3f(1,0,0);
		up = new Vector3f(0,1,0);
	}
	
	public Camera(Vector3f position, Vector3f direction, Vector3f up){
		this.position = position;
	}
	
	public Matrix4f transform(){
		return Matrix4f.view(position, direction, up);
	}
	
}
