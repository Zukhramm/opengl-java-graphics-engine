package com.thecrouchmode.graphics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.glu.GLU;

import com.thecrouchmode.LWJGL.util.ResourceReader;
import com.thecrouchmode.vector.Matrix4f;
import com.thecrouchmode.vector.Vector3f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL21.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL31.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.GL33.*;
import static org.lwjgl.opengl.GL40.*;
import static org.lwjgl.opengl.GL41.*;
import static org.lwjgl.opengl.GL42.*;
import static org.lwjgl.opengl.GL43.*;


public class Shader{
	
	public final int id;
	private final Map<String, Integer> locations = new HashMap<>();
	
	public final static int VERTEX = GL_VERTEX_SHADER;
	public final static int FRAGMENT = GL_FRAGMENT_SHADER;
	public final static int GEOMETRY = GL_GEOMETRY_SHADER;
	
	public Shader(String vertexPath, String fragmentPath, String geometryPath) throws IOException{
		ArrayList<Integer> shaders = new ArrayList<>();
		if(vertexPath != null) shaders.add(loadShader(vertexPath, VERTEX));
		if(fragmentPath != null) shaders.add(loadShader(fragmentPath, FRAGMENT));
		if(geometryPath != null) shaders.add(loadShader(geometryPath, GEOMETRY));	
		int[] ids = new int[shaders.size()];
		for(int i = 0; i < ids.length; i++) ids[i] = shaders.get(i);
		id = Shader.createProgram(ids);
		for(int shader : shaders) glDeleteShader(shader);
		if (id == 0) throw new IOException("Unable to create shader program");
	}
	
	public Shader(int vertexId, int fragmentId, int geometryId) throws IOException{
		ArrayList<Integer> shaders = new ArrayList<>();
		if(vertexId != 0) shaders.add(vertexId, VERTEX);
		if(fragmentId != 0) shaders.add(fragmentId, FRAGMENT);
		if(geometryId != 0) shaders.add(geometryId, GEOMETRY);	
		int[] ids = new int[shaders.size()];
		for(int i = 0; i < ids.length; i++) ids[i] = shaders.get(i);
		id = Shader.createProgram(ids);
		if (id == 0) throw new IOException("Unable to create shader program");
	}
	
	public void use(){
		glUseProgram(id);
	}
	
	public static void unbind(){
		glUseProgram(0);
	}
	
	public void setUniform(String name, Matrix4f uniform) throws ShaderException{
		if(!locations.containsKey(name)){
			locations.put(name, glGetUniformLocation(id, name));
			if(locations.get(name) == -1) throw new ShaderException("Unable to get uniform location");
		}
		glUniformMatrix4(locations.get(name), false, uniform.buffer());
	}
	
	public void setUniform(String name, Vector3f uniform) throws ShaderException{
		if(!locations.containsKey(name)){
			locations.put(name, glGetUniformLocation(id, name));
			if(locations.get(name) == 0) throw new ShaderException("Unable to get uniform location");
		}
		glUniform3f(locations.get(name), uniform.x, uniform.y, uniform.z);
	}
	
	public void setUniform(String name, float uniform) throws ShaderException{
		if(!locations.containsKey(name)){
			locations.put(name, glGetUniformLocation(id, name));
			if(locations.get(name) == 0) throw new ShaderException("Unable to get uniform location");
		}
		glUniform1f(locations.get(name), uniform);
	}

	public void setUniform(String name, int uniform) throws ShaderException{
		if(!locations.containsKey(name)){
			locations.put(name, glGetUniformLocation(id, name));
			if(locations.get(name) == 0) throw new ShaderException("Unable to get uniform location");
		}
		glUniform1i(locations.get(name), uniform);
	}
	
	
	private static int loadShader(String path, int type) throws IOException{
		StringBuilder sourceBuilder = new StringBuilder();
		
		for(String line : ResourceReader.readLines(path)){
			sourceBuilder.append(line).append('\n');
		}
		
		int program = glCreateProgram();
		
		if(program == 0){
			throw new ShaderException("Unable to create shader.");
		}
		
		int shader = glCreateShader(type);
		glShaderSource(shader, sourceBuilder.toString());
		glCompileShader(shader);
		
		if(glGetShaderi(shader, GL_COMPILE_STATUS) == GL_FALSE){
			System.out.println(glGetShaderInfoLog(
					shader,
					glGetShaderi(shader, GL_INFO_LOG_LENGTH)));
		}
		return shader;
	}
	
	private static int createProgram(int ... shaders) throws IOException{
		int program = glCreateProgram();
		
		if(program == 0){
			throw new ShaderException("Unable to create program.");
		}
		
		for(int shader : shaders){
			glAttachShader(program, shader);
		}
		
		glLinkProgram(program);
		//glValidateProgram(program);
		if(glGetProgrami(program, GL_LINK_STATUS) == GL_FALSE){
			System.out.println(glGetProgramInfoLog(
					program,
					glGetProgrami(program, GL_INFO_LOG_LENGTH)));
		}
		
		return program;
	}
	
//	public static int createProgram(String vertexShaderSourcePath, String fragmentShaderSourcePath, String geometryShaderSourcePath) throws IOException{
//		int vertex, fragment, geometry;
//		
//		ArrayList shaders = new ArrayList<Integer>();
//		if(vertexShaderSourcePath != null){
//			vertex = loadShader(vertexShaderSourcePath, VERTEX);
//			shaders.add(vertex);
//		}
//		if(vertexShaderSourcePath != null){
//			fragment = loadShader(fragmentShaderSourcePath, FRAGMENT);
//			shaders.add(fragment);
//		}
//		if(vertexShaderSourcePath != null){
//			geometry = loadShader(geometryShaderSourcePath, GEOMETRY);
//			shaders.add(geometry);
//		}
//	}
}
