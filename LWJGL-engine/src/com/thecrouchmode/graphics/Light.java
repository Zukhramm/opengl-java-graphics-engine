package com.thecrouchmode.graphics;

import com.thecrouchmode.vector.Vector3f;
import com.thecrouchmode.vector.Vector4f;

public class Light{
   public Vector4f pos;
   public Vector3f intensity;
   
   public Light(Vector4f pos, Vector3f intensity){
   	this.pos = pos;
   	this.intensity = intensity;
   }
   
   public static Builder builder(){
      return new Builder();
   } 
   
   public static class Builder{
      private Vector4f pos;
      private Vector3f intensity;
      private boolean posSet = false;
      private boolean intensitySet = false;
      
      private Builder(){
      	
      }
      
      public Builder pos(Vector4f pos){
			this.pos = pos;
			posSet = true;
      	return this;      	
      }
      
      public Builder pos(Vector3f pos){
			return pos(new Vector4f(pos, 1));   	
      }
      
      public Builder pos(float x, float y, float z){
			return pos(new Vector4f(x, y, z, 1));   	
      }
      
      public Builder intensity(Vector3f intensity){
      	this.intensity = intensity;
      	intensitySet = true;
			return this;
		}
      
      public Builder intensity(float r, float g, float b){
			return intensity(new Vector3f(r, g, b));
		}
      
      public Light build(){
      	if(!posSet || !intensitySet) throw new IllegalStateException(
      			"Not all parameters set in builder");
      	return new Light(pos, intensity);
      }

		public Builder intensity(double r, double g, double b){
			return intensity(new Vector3f(r, g, b));
		}
      
   }

}
