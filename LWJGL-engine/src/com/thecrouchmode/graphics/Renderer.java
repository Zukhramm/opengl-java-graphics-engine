package com.thecrouchmode.graphics;

import static org.lwjgl.opengl.GL11.*;
//import static org.lwjgl.opengl.GL12.*;
//import static org.lwjgl.opengl.GL13.*;
//import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
//import static org.lwjgl.opengl.GL21.*;
import static org.lwjgl.opengl.GL30.*;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class Renderer{
	
	public static void createWindow(final int width, final int height, final String title) throws LWJGLException{
		Display.setDisplayMode(new DisplayMode(width, height));
		Display.create();
		System.out.println();
		Display.setTitle("tutorial");
	}
	
	public static String version(){
		return glGetString(GL_VERSION);
	}
	
	public static void clear(){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// Clear the screen and depth buffer
	}
	
	public static void init(){
		glFrontFace(GL_CW);
		glCullFace(GL_FRONT);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_FRAMEBUFFER_SRGB);
		glClearColor(0,0,0,0);
		glShadeModel(GL_SMOOTH);
		glClearDepth(1);
	}
}

