package com.thecrouchmode.graphics;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.OpenGLException;
import org.newdawn.slick.opengl.TextureLoader;

public class Texture{
	
	private final int id;
	
	public Texture(final String path, final String extension) throws IOException{
		id = load(path, extension);
	}
	
	public static int load(final String path) throws IOException{
		try(FileInputStream stream = new FileInputStream(path)){
			return TextureLoader.getTexture("dds", stream, false).getTextureID();
		}
		catch(IOException e){
			throw e;
		}
	}
	public static int load(final String path, final String extension) throws IOException{
		try(FileInputStream stream = new FileInputStream(path)){
			return TextureLoader.getTexture(extension, stream, false).getTextureID();
		}
		catch(IOException e){
			throw e;
		}
	}
	
	public void bind(){
		glBindTexture(GL_TEXTURE_2D, id);
	}
	
	public static void unbind(){
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public static int glTexture(int i){
		if(i >= GL20.GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS){
			throw new OpenGLException("Number of textures exceeds texture limit");
		}
		return GL_TEXTURE0 + i;
	}
}
